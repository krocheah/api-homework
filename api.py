import requests

BASE_URL = "https://sandbox-reporting.rpdpymnt.com/api/v3/"
USERNAME = "merchant@test.com"
PASSWORD = "123456"

class ReportingApi:
    def get_token():
        url = BASE_URL + "merchant/user/login"
        data = {
            "email" : USERNAME,
            "password": PASSWORD,
        }

        response = requests.post(url = url, data = data)

        if response.status_code == 200 and response.json().get("status") == "APPROVED":
            return response.json().get("token")
        return None