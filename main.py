from flask import Flask, render_template
from api import ReportingApi

app = Flask(__name__)

@app.route('/')
def index():
    data = {
        "token": ReportingApi.get_token()
    }

    return render_template("index.html", data = data)

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = 8080, debug = True)
